import CategoriesItem from "./category-item";
import {db} from "../_lib/prisma";

const CardCategories = async () => {
    const categories = await db.category.findMany({});

    return (
        <div className="py-2 flex w-full gap-3 overflow-y-auto" style={{
            WebkitOverflowScrolling: 'touch',
            scrollbarWidth: 'none',
            msOverflowStyle: 'none'}}>
            {categories.map((categories) => {
                return (
                    <CategoriesItem key={categories.id} categories={categories}/>
                )
            })}
        </div>
    );
};

export default CardCategories;