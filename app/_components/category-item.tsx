import { Category } from "@prisma/client";

import Image  from "next/image";
import Link from "next/link";

interface CategoriesItemsProps {
    categories: Category
}

const CategoriesItem = ({categories}: CategoriesItemsProps) => {

    return (
        <Link href={`/categories/${categories.id}/products`} className="flex items-center justify-center gap-3 rounded-full bg-white px-4 py-3 shadow-md">
            <div className="flex items-center justify-center min-w-30 h-12 rounded-full py-4 px-5 gap-3">
                <Image src={categories.imageUrl} alt={categories.name} height={40} width={20} />
                <span className="font-sans font-sm leading-5 font-semibold"> {categories.name} </span>
            </div>
        </Link>
    )
};

export default CategoriesItem;